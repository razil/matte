package com.razil.matte.repo

import com.razil.matte.api.TmdbService
import com.razil.matte.data.model.account.Login
import com.razil.matte.data.model.auth.Session
import io.reactivex.Single
import javax.inject.Inject

class UserRepo @Inject constructor(private val tmdbService: TmdbService) {
    fun doLogin(apiKey: String, username: String, password: String): Single<Session> {
        return tmdbService.requestToken(apiKey).flatMap {
            tmdbService.createSession(
                apiKey,
                Login(username, password, it.requestToken)
            )
        }
    }
}