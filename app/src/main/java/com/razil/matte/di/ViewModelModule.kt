package com.razil.matte.di

import com.razil.matte.repo.UserRepo
import com.razil.matte.ui.login.LoginViewModelFactory
import dagger.Module
import dagger.Provides

@Module
class ViewModelModule {
    @Provides
    fun provideViewModelFactory(userRepo: UserRepo) = LoginViewModelFactory(userRepo)
}