package com.razil.matte.di

import android.app.Application
import com.razil.matte.BuildConfig
import com.razil.matte.api.TmdbService
import com.razil.matte.repo.UserRepo
import com.squareup.moshi.Moshi
import com.squareup.moshi.kotlin.reflect.KotlinJsonAdapterFactory
import dagger.Module
import dagger.Provides
import okhttp3.Cache
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import okhttp3.logging.HttpLoggingInterceptor.Level.BODY
import okhttp3.logging.HttpLoggingInterceptor.Level.NONE
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.moshi.MoshiConverterFactory
import javax.inject.Singleton

@Module
class NetModule(private val baseUrl: String) {
    @Provides
    @Singleton
    fun provideOkHttpCache(app: Application): Cache = Cache(app.cacheDir, 10 * 1024 * 1024)

    @Provides
    @Singleton
    fun provideOkHttpClient(cache: Cache): OkHttpClient {
        val interceptor = HttpLoggingInterceptor(HttpLoggingInterceptor.Logger {

        }).apply {
            level = if (BuildConfig.DEBUG) BODY else NONE
        }

        return OkHttpClient.Builder().addInterceptor(interceptor).cache(cache).build()
    }

    @Provides
    @Singleton
    fun provideMoshi(): Moshi = Moshi.Builder().add(KotlinJsonAdapterFactory()).build()

    @Provides
    @Singleton
    fun provideTmdbService(moshi: Moshi, client: OkHttpClient): TmdbService {
        return Retrofit.Builder()
            .baseUrl(baseUrl)
            .client(client)
            .addConverterFactory(MoshiConverterFactory.create(moshi))
            .addCallAdapterFactory(RxJava2CallAdapterFactory.createAsync())
            .build()
            .create(TmdbService::class.java)
    }

    @Provides
    @Singleton
    fun provideUserRepo(tmdbService: TmdbService) = UserRepo(tmdbService)

}