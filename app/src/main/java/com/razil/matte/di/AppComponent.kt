package com.razil.matte.di

import com.razil.matte.ui.login.LoginFragment
import dagger.Component
import javax.inject.Singleton

@Singleton
@Component(modules = [AppModule::class, NetModule::class, ViewModelModule::class])
interface AppComponent {
    fun inject(fragment: LoginFragment)
}