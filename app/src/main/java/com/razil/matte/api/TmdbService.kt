package com.razil.matte.api

import com.razil.matte.data.model.account.Login
import com.razil.matte.data.model.auth.Session
import com.razil.matte.data.model.auth.Token
import io.reactivex.Single
import retrofit2.http.Body
import retrofit2.http.GET
import retrofit2.http.POST
import retrofit2.http.Query

interface TmdbService {
    @GET("authentication/token/new")
    fun requestToken(@Query("api_key") apiKey: String): Single<Token>

    @POST("authentication/token/validate_with_login")
    fun createSession(@Query("api_key") apiKey: String, @Body login: Login): Single<Session>
}