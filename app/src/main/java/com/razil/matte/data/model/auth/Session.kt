package com.razil.matte.data.model.auth

import com.squareup.moshi.Json

data class Session(
    @Json(name = "expires_at")
    val expiresAt: String,
    @Json(name = "request_token")
    val requestToken: String,
    @Json(name = "success")
    val success: Boolean
)