package com.razil.matte.data.model.account

import com.squareup.moshi.Json

data class Login(
    @Json(name = "username")
    val username: String,
    @Json(name = "password")
    val password: String,
    @Json(name = "request_token")
    val requestToken: String
)