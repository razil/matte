package com.razil.matte.data.model.auth

import com.squareup.moshi.Json

data class Token(
    @Json(name = "expires_at")
    val expiresAt: String,
    @Json(name = "request_token")
    val requestToken: String,
    @Json(name = "success")
    val success: Boolean
)