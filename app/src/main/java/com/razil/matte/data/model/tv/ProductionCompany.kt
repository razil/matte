package com.razil.matte.data.model.tv

import com.squareup.moshi.Json

data class ProductionCompany(
    @Json(name = "id")
    val id: Int?,
    @Json(name = "logo_path")
    val logoPath: Any?,
    @Json(name = "name")
    val name: String?,
    @Json(name = "origin_country")
    val originCountry: String?
)