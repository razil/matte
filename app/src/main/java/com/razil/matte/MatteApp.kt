package com.razil.matte

import android.app.Application
import com.razil.matte.di.*
import timber.log.Timber

class MatteApp : Application() {
    lateinit var appComponent: AppComponent

    override fun onCreate() {
        super.onCreate()

        Timber.plant(Timber.DebugTree())

        appComponent = DaggerAppComponent.builder()
            .appModule(AppModule(this))
            .netModule(NetModule(getString(R.string.tmdb_api_base_url)))
            .viewModelModule(ViewModelModule())
            .build()
    }
}