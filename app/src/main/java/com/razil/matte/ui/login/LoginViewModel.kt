package com.razil.matte.ui.login

import androidx.lifecycle.ViewModel
import com.razil.matte.data.model.auth.Session
import com.razil.matte.repo.UserRepo
import io.reactivex.Single
import javax.inject.Inject

class LoginViewModel @Inject constructor(private val userRepo: UserRepo) : ViewModel() {
    fun doLogin(apiKey: String, username: String, password: String): Single<Session> {
        return userRepo.doLogin(apiKey, username, password)
    }
}