package com.razil.matte.ui.login

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProviders
import com.razil.matte.MatteApp
import com.razil.matte.R
import com.razil.matte.api.TmdbService
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import kotlinx.android.synthetic.main.fragment_login.*
import timber.log.Timber
import javax.inject.Inject


class LoginFragment : Fragment() {

    @Inject
    lateinit var tmdbService: TmdbService
    @Inject
    lateinit var loginViewModelFactory: LoginViewModelFactory

    private val disposable: CompositeDisposable = CompositeDisposable()

    private val username = "razilsh"
    private val password = "7344MdmPnCNuaNIkLYyC"

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_login, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        (requireActivity().application as MatteApp).appComponent.inject(this)

        val loginViewModel =
            ViewModelProviders.of(this, loginViewModelFactory).get(LoginViewModel::class.java)

        tilUsername.editText?.setText(username)
        tilPassword.editText?.setText(password)


        buttonLogin.setOnClickListener {
            disposable.add(
                loginViewModel.doLogin(
                    getString(R.string.tmdb_api_key),
                    username,
                    password
                ).observeOn(AndroidSchedulers.mainThread()).subscribe { session, error ->
                    Timber.i(session?.toString())
                    Timber.e(error)
                })
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        disposable.dispose()
    }
}