package com.razil.matte.ui.login

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.razil.matte.repo.UserRepo
import javax.inject.Inject

class LoginViewModelFactory @Inject constructor(private val userRepo: UserRepo) :
    ViewModelProvider.Factory {
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return if (modelClass.isAssignableFrom(LoginViewModel::class.java)) {
            LoginViewModel(userRepo) as T
        } else {
            throw IllegalArgumentException("ViewModel not found")
        }
    }
}